import 'package:flutter/material.dart';

import '../shared_prefs/user_prefs.dart';
import '../widgets/menu_widget.dart';

class HomePage extends StatelessWidget {
  static const String routeName = 'home';
  final UserPrefs _userPrefs = UserPrefs();

  @override
  Widget build(BuildContext context) {
    _userPrefs.ultimaPagina = HomePage.routeName;

    return Scaffold(
      appBar: AppBar(
        title: Text('Preferencias de Usuario'),
        backgroundColor: _userPrefs.colorSecundario
            ? Colors.teal
            : Theme.of(context).primaryColor,
      ),
      drawer: MenuWidget(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Color Secundario: ${_userPrefs.colorSecundario}'),
          Divider(),
          Text('Género: ${_userPrefs.genero}'),
          Divider(),
          Text('Nombre de Usuario: ${_userPrefs.nombre}'),
          Divider(),
        ],
      ),
    );
  }
}
