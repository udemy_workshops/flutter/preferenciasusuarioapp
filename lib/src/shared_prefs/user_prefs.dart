import 'package:shared_preferences/shared_preferences.dart';

import '../pages/home_page.dart';

class UserPrefs {
  static final UserPrefs _singleton = new UserPrefs._();

  UserPrefs._();

  factory UserPrefs() {
    return _singleton;
  }

  SharedPreferences _sharedPrefs;

  Future<void> init() async {
    if (_sharedPrefs == null)
      _sharedPrefs = await SharedPreferences.getInstance();
  }

  int get genero {
    return _sharedPrefs?.getInt('genero') ?? 1;
  }

  set genero(int value) {
    _sharedPrefs?.setInt('genero', value);
  }

  bool get colorSecundario {
    return _sharedPrefs?.getBool('color_secundario') ?? false;
  }

  set colorSecundario(bool value) {
    _sharedPrefs?.setBool('color_secundario', value);
  }

  String get nombre {
    return _sharedPrefs?.getString('nombre') ?? '';
  }

  set nombre(String value) {
    _sharedPrefs?.setString('nombre', value);
  }

  String get ultimaPagina {
    return _sharedPrefs?.getString('ultima_pagina') ?? HomePage.routeName;
  }

  set ultimaPagina(String value) {
    _sharedPrefs?.setString('ultima_pagina', value);
  }
}
