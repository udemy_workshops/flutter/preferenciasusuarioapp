import 'package:flutter/material.dart';

import 'src/shared_prefs/user_prefs.dart';
import 'src/pages/home_page.dart';
import 'src/pages/settings_page.dart';

Future<void> main() async {
  final UserPrefs userPrefs = UserPrefs();
  // bug del paquete shared_preferences:
  // If you're running an application and need to access the binary messenger before `runApp()` has been called (for example, during plugin initialization), then you need to explicitly call the `WidgetsFlutterBinding.ensureInitialized()` first.
  // If you're running a test, you can call the `TestWidgetsFlutterBinding.ensureInitialized()` as the first line in your test's `main()` method to initialize the binding.
  WidgetsFlutterBinding.ensureInitialized();
  await userPrefs.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final UserPrefs _userPrefs = UserPrefs();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Preferencias',
      initialRoute: _userPrefs.ultimaPagina,
      routes: <String, Widget Function(BuildContext)>{
        HomePage.routeName: (BuildContext context) => HomePage(),
        SettingsPage.routeName: (BuildContext context) => SettingsPage(),
      },
    );
  }
}
