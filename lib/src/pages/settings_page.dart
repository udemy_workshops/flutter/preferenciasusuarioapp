import 'package:flutter/material.dart';

import '../shared_prefs/user_prefs.dart';
import '../widgets/menu_widget.dart';

class SettingsPage extends StatefulWidget {
  static const String routeName = 'settings';

  @override
  _SettingsPage createState() => _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {
  final UserPrefs _userPrefs = UserPrefs();

  TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = new TextEditingController(text: _userPrefs.nombre);
  }

  void _setSelectedRadio(int value) {
    setState(() {
      _userPrefs.genero = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    _userPrefs.ultimaPagina = SettingsPage.routeName;

    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
        backgroundColor: _userPrefs.colorSecundario
            ? Colors.teal
            : Theme.of(context).primaryColor,
      ),
      drawer: MenuWidget(),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Settings',
              style: TextStyle(
                fontSize: 45.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Divider(),
          SwitchListTile(
            value: _userPrefs.colorSecundario,
            title: Text('Color Secundario'),
            onChanged: (bool value) => setState(() {
              _userPrefs.colorSecundario = value;
            }),
          ),
          RadioListTile(
            value: 1,
            title: Text('Masculino'),
            groupValue: _userPrefs.genero,
            onChanged: _setSelectedRadio,
          ),
          RadioListTile(
            value: 2,
            title: Text('Femenino'),
            groupValue: _userPrefs.genero,
            onChanged: _setSelectedRadio,
          ),
          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textEditingController,
              decoration: InputDecoration(
                labelText: 'Nombre',
                helperText: 'Nombre de la Persona usando el Teléfono',
              ),
              onChanged: (String value) => setState(() {
                _userPrefs.nombre = value;
              }),
            ),
          ),
        ],
      ),
    );
  }
}
